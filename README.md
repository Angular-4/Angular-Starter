# Angular Starter
Materials from the ["Angular: Getting Started"](http://bit.ly/Angular-GettingStarted) course on Pluralsight.

The starter files set up for use in VSCode, WebStorm, or other editors.


NOTE: The installation was tested using node v6.5.0 and npm 3.10.6.